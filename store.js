#! /usr/bin/env node

//Requiring modules
var fs = require('fs');
var path = require('path');

//Physical store file name
const storeFileName = 'store.db';

//#region Help messages
const helpMessage = `
Usage: node store.js <command>

where command is one of:
    add, list, get, remove, clear`;
const helpMessageAdd = `
Error: Incorrect command.

Usage: node store.js add <key> <value>`;
const helpMessageRemove = `
Error: Incorrect command.

Usage: node store.js remove <key>`;
const helpMessageGet = `
Error: Incorrect command.

Usage: node store.js get <key>`;
//#endregion


if (process.argv.length > 2) {
    switch (process.argv[2]) {
        case 'add':
            addToStore(process.argv);
            break;
        case 'list':
            listStore();
            break;
        case 'get':
            getFromStore(process.argv);
            break;
        case 'remove':
            removeFromStore(process.argv);
            break;
        case 'clear':
            clearStore();
            break;
        default:
            console.log(helpMessage);
    }
} else {
    console.log(helpMessage);
}

//#region Filesystem functions
function getStore(fileName) {
    var store;
    var filePath = path.join(__dirname, fileName);
    if (fs.existsSync(filePath))
        store = JSON.parse(fs.readFileSync(filePath));
    else {
        store = new Object();
        fs.writeFileSync(fileName, JSON.stringify(store), function (err) {
            if (err) throw err;
        });
    }
    return store;
}
//#endregion

//#region Store operations
function addToStore(args) {
    if (args.length > 3) {
        var store = getStore(storeFileName);
        store[args[3]] = args[4] ? args[4] : '';
        saveChanges(store);
        console.log(`"${args[3]}" has been added/updated`);
    } else
        console.log(helpMessageAdd);
}

function listStore() {
    var store = getStore(storeFileName);
    console.log(store);
}

function getFromStore(args) {
    if (args.length > 3) {
        var store = getStore(storeFileName);
        if (store[args[3]] != undefined) {
            console.log(`${args[3]}:${store[args[3]]}`);
        } else
            console.log('Key does not exist')
    } else
        console.log(helpMessageGet);
}

function removeFromStore(args) {
    if (args.length > 3) {
        var store = getStore(storeFileName);
        if (store[args[3]] != undefined) {
            delete store[args[3]];
            saveChanges(store);
            console.log(`"${args[3]}" has been removed`);
        } else
            console.log('Key does not exist');
    } else
        console.log(helpMessageRemove);
}

function clearStore() {
    var store = getStore(storeFileName);
    store = new Object();
    saveChanges(store);
    console.log('Store has been cleared');
}

function saveChanges(storeContent) {
    fs.writeFile(storeFileName, JSON.stringify(storeContent), function (err) {
        if (err) throw err;
    });
}
//#endregion